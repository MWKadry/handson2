package Storage;
import com.sumergeprogram.Student;

import java.util.*;

public class StudentCollection {


    public static ArrayList <Student> StudentList =
            new ArrayList<Student>(Arrays.asList(new Student("amr","madinaty", "amr@gmail.com", 123),
            new Student("omar","rehab", "omar@gmail.com", 1234)));



    public static void delete(long Id) {
        for (int i = 0; i < StudentList.size(); i++) {
            if (StudentList.get(i).getId() == Id) {
                StudentList.remove(StudentList.get(i));
                break;

            }
        }
    }

    public static ArrayList<Student> getAllStudents(String Name, String Email, String Address){

       ArrayList <Student> foundStudents = new ArrayList<Student>();

        for (int i = 0; i < StudentList.size(); i++) {
            if (Name != null && !StudentList.get(i).getName().equals(Name)) {
                System.out.println(Name);
                continue;
            }
            if (Email != null && !StudentList.get(i).getEmail().equals(Email)) {
                continue;
            }
            if (Address != null && !StudentList.get(i).getAddress().equals(Address)) {
                continue;
            }
            foundStudents.add(StudentList.get(i));
        }
        return foundStudents;
    }

    public static Student searchStudentId(long Id) {
        for (int i = 0; i < StudentList.size(); i++) {
            if (StudentList.get(i).getId() == Id) {
                return StudentList.get(i);
            }
        }
        return null;
    }

    public static Student searchStudentName(String Name) {
        for (int i = 0; i < StudentList.size(); i++) {
            if (StudentList.get(i).getName().equals(Name)) {
                return StudentList.get(i);
            }
        }
        return null;
    }


    public static Student searchStudentAdress(String Address) {
        for (int i = 0; i < StudentList.size(); i++) {
            if (StudentList.get(i).getAddress().equals(Address)) {
                return StudentList.get(i);
            }
        }
        return null;
    }

    public static void StudentUpdate(long Id, String Name, String Address, String Email) {
        for (int i = 0; i < StudentList.size(); i++) {
            if (StudentList.get(i).getId()==Id){

                StudentList.get(i).setName(Name);
                StudentList.get(i).setAddress(Address);
                StudentList.get(i).setEmail(Email);

                break;

            }
        }

    }


}