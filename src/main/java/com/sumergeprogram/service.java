package com.sumergeprogram;

import Storage.StudentCollection;
import javax.ws.rs.FormParam;
//import sun.security.provider.certpath.ResponderId;

import javax.enterprise.context.RequestScoped;
import javax.naming.Name;
import javax.persistence.Id;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("students")

public class service {

    private static final Logger LOGGER = Logger.getLogger(StudentCollection.class.getName());

    static StudentCollection students = new StudentCollection();


    @GET
    @Path("{id}")
    public Response getStudent(@PathParam("id") Long Id) {
        try {
            return Response.ok().
                    entity(StudentCollection.searchStudentId(Id)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
            }
    }

    @DELETE
    @Path("{id}")
    public Response deleteStudent(@PathParam("id") Long Id) {
        try {
            StudentCollection.delete(Id);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }


    @GET
    public Response getAllStudents(@QueryParam("name") String Name,
                                   @QueryParam("email") String Email,
                                   @QueryParam("address") String Address) {


        try {
            return Response.ok().
                    entity(StudentCollection.getAllStudents(Name,Email,Address)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @POST
    @Path("edit")
    public Response editStudent(Student student) {


        try {

            StudentCollection.StudentUpdate(student.getId(),student.getName(),student.getAddress(),student.getEmail());


            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

}
