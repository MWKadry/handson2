package com.sumergeprogram;

public class Student {
    String Name;
    String Address;
    String Email;
    long Id;

    public Student(String name, String address, String email, long id) {
        this.Name = name;
        this.Address = address;
        this.Email = email;
        this.Id = id;
    }
    public Student() {
    }

    public String getName() {
        return Name;
    }

    public String getAddress() {
        return Address;
    }

    public String getEmail() {
        return Email;
    }

    public Long getId() {
        return Id;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setId(long id) {
        Id = id;
    }



}
